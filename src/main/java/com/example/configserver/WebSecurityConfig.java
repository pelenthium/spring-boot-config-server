package com.example.configserver;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Value("${management.endpoints.encrypt.disable:false}")
  private boolean disable_encrypt;
  
  @Value("${management.endpoints.decrypt.disable:false}")
  private boolean disable_decrypt;
  
  @Value("${management.endpoints.error.disable:false}")
  private boolean disable_error;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    
    ArrayList endpoints = new ArrayList();
    if(disable_encrypt){
      endpoints.add("/encrypt/**");
    }
    
    if(disable_decrypt){
      endpoints.add("/decrypt/**");
    }
    
    if(disable_error){
      endpoints.add("/error/**");
    }
    
    String[] perm = new String[endpoints.size()];
    perm = (String[]) endpoints.toArray(perm);
    http
      //Disable csrf
      .csrf()
      .disable()
      .authorizeRequests()
      //Disable endpoints specified       
      .antMatchers(perm)
      .denyAll()
      .anyRequest()
      .authenticated()
      .and()
      .httpBasic()
    ;
  }
}